package aze.app.valyuta.di.singleton_modules

import az.reviziya.mobile.dagger.modules.NavigationModule
import aze.app.valyuta.di.main_activity_modules.MainModule
import aze.app.valyuta.di.annotation.MainScope
import aze.app.valyuta.di.main_activity_modules.database.DatabaseProvideModule
import aze.app.valyuta.di.main_activity_modules.fragments.MainFragmentBuildersModule
import aze.app.valyuta.di.main_activity_modules.fragments.MainViewModelsModule
import aze.app.valyuta.di.main_activity_modules.network.RetrofitProviderModule
import aze.app.valyuta.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @MainScope
    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class,NavigationModule::class,MainViewModelsModule::class, MainModule::class, RetrofitProviderModule::class, DatabaseProvideModule::class])
    abstract fun contributesInjectMainActivity(): MainActivity
}
