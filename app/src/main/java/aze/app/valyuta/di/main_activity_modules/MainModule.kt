package aze.app.valyuta.di.main_activity_modules

import aze.app.valyuta.di.annotation.MainScope
import aze.app.valyuta.entity.Icons
import dagger.Module
import dagger.Provides

@Module
class MainModule {
    @Provides
    @MainScope
    fun getIcons(): Icons = Icons()
}