package aze.app.valyuta.di.singleton_modules

import android.app.Application
import androidx.room.Room
import aze.app.valyuta.di.annotation.MainScope
import aze.app.valyuta.persistance.AppDatabase
import aze.app.valyuta.persistance.currencies.CurrencyDao
import aze.app.valyuta.persistance.currencies.LocalCurrency
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "database")
            .fallbackToDestructiveMigration()
            .build()
    }
}