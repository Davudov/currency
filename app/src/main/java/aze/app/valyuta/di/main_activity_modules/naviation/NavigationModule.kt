package az.reviziya.mobile.dagger.modules

import aze.app.valyuta.di.annotation.MainScope
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


@Module
class NavigationModule {

    private val cicerone: Cicerone<Router> = Cicerone.create()

    @MainScope
    @Provides
    fun provideRouter(): Router = cicerone.router

    @MainScope
    @Provides
    fun navigationHolder(): NavigatorHolder = cicerone.navigatorHolder

}
