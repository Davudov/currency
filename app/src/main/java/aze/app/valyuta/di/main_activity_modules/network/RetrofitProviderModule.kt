package aze.app.valyuta.di.main_activity_modules.network

import aze.app.valyuta.di.annotation.MainScope
import aze.app.valyuta.network.services.CurrencyService
import dagger.BindsInstance
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Inject

@Module
class RetrofitProviderModule {

    @MainScope
    @Provides
    @Inject
    fun provideCurrencyService(retrofit: Retrofit): CurrencyService =
        retrofit.create(CurrencyService::class.java)


}