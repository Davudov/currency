package aze.app.valyuta.di.main_activity_modules.fragments

import androidx.lifecycle.ViewModel
import aze.app.valyuta.di.annotation.ViewModelKey
import aze.app.valyuta.ui.main.history.HistoryViewModel
import aze.app.valyuta.ui.main.today.TodayViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(TodayViewModel::class)
    abstract fun bindTodayViewModel(todayViewModel: TodayViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    abstract fun bindHistoryViewModel(historyViewModel: HistoryViewModel): ViewModel

}