package aze.app.valyuta.di.component

import android.app.Application
import aze.app.valyuta.App
import aze.app.valyuta.di.singleton_modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,AppModule::class,ActivityBuilderModule::class,ViewModelFactoryModule::class,NetworkModule::class, DatabaseModule::class])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}
