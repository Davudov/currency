package aze.app.valyuta.di.singleton_modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import aze.app.valyuta.factory.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Module
class ViewModelFactoryModule {
    @Provides
    @Inject
    fun viewModelFactory(providerMap: Map<Class<out ViewModel>, Provider<ViewModel>>): ViewModelProvider.Factory{
        return AppViewModelFactory(providerMap)
    }
}