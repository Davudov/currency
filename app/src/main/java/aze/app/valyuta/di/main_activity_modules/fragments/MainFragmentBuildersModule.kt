package aze.app.valyuta.di.main_activity_modules.fragments

import aze.app.valyuta.ui.main.history.HistoryFragment
import aze.app.valyuta.ui.main.today.TodayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {
    @ContributesAndroidInjector()
     abstract fun contributesInjectTodayFragment(): TodayFragment

    @ContributesAndroidInjector()
     abstract fun contributesInjectHistoryFragment(): HistoryFragment
}