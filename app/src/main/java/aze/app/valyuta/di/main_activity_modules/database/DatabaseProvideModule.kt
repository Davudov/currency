package aze.app.valyuta.di.main_activity_modules.database

import aze.app.valyuta.di.annotation.MainScope
import aze.app.valyuta.persistance.AppDatabase
import aze.app.valyuta.persistance.currencies.CurrencyDao
import aze.app.valyuta.persistance.currencies.LocalCurrency
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class DatabaseProvideModule {

    @Provides
    @MainScope
    @Inject
    fun provideProfileDao(appDatabase: AppDatabase): CurrencyDao {
        return appDatabase.currencyDao()
    }

    @Provides
    @MainScope
    @Inject
    fun provideLocalCurrency(currencyDao: CurrencyDao): LocalCurrency {
        return LocalCurrency(currencyDao)
    }


}