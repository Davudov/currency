package aze.app.valyuta.network.services

import aze.app.valyuta.entity.ValCurs
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface CurrencyService {
    @GET("currencies/{today}.xml")
    fun getTodayData(@Path("today") today:String):Single<ValCurs>
}