//package aze.app.valyuta.adapter
//
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.databinding.DataBindingUtil
//import aze.app.valyuta.R
//import aze.app.valyuta.callback.ItemClickListener
//import aze.app.valyuta.databinding.ListChildItemBinding
//import aze.app.valyuta.entity.Valuate
//
//class ValuteAdapter(context: Context, itemClickListener: ItemClickListener) :
//    BaseAdapter<Valuate, ValuteAdapter.MainViewHolder>(context, itemClickListener) {
//
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): MainViewHolder =
//        MainViewHolder(
//            DataBindingUtil.inflate(
//                LayoutInflater.from(parent.context),
//                R.layout.list_child_item,
//                parent,
//                false
//            )
//        )
//
//
//    class MainViewHolder(val binding: ListChildItemBinding) :
//        BaseAdapter.Holder<Valuate>(binding.root) {
//
//        override fun unbind() {
//
//        }
//
//        override fun bind(item: Valuate) {
//            binding.item = item
//            binding.executePendingBindings()
//        }
//
//
//    }
//}