//package aze.app.valyuta.adapter
//
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.databinding.DataBindingUtil
//import androidx.recyclerview.widget.LinearLayoutManager
//import aze.app.valyuta.R
//import aze.app.valyuta.callback.ItemClickListener
//import aze.app.valyuta.databinding.ListItemBinding
//import aze.app.valyuta.entity.ValType
//
//class ValTypesAdapter(context: Context, itemClickListener: ItemClickListener) :
//    BaseAdapter<ValType, ValTypesAdapter.MainViewHolder>(context, itemClickListener) {
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
//        MainViewHolder(
//            itemClickListener!!, context,
//            DataBindingUtil.inflate(
//                LayoutInflater.from(parent.context),
//                R.layout.list_item,
//                parent,
//                false
//            )
//        )
//
//    class MainViewHolder(
//        val itemClickListener: ItemClickListener,
//        val context: Context,
//        val binding: ListItemBinding
//    ) : BaseAdapter.Holder<ValType>(binding.root) {
//
//        override fun bind(item: ValType) {
//            binding.item = item
//            val adapter =
//                ValuteAdapter(context = context, itemClickListener = itemClickListener)
//            adapter.addAll(item.valuateList)
//            binding.childRecycler.adapter = adapter
//            binding.childRecycler.itemAnimator = null
//            binding.childRecycler.layoutManager =
//                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//            binding.executePendingBindings()
//        }
//
//        override fun unbind() {
//
//        }
//
//    }
//}