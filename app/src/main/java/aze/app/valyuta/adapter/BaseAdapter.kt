//package aze.app.valyuta.adapter
//
//import aze.app.valyuta.callback.ItemClickListener
//
//import android.content.Context
//import android.view.View
//import androidx.recyclerview.widget.RecyclerView
//import java.util.*
//
//abstract class BaseAdapter<E, Holder : BaseAdapter.Holder<E>> : RecyclerView.Adapter<Holder> {
//    var context: Context
//
//    var items: MutableList<E>
//
//    var backup: List<E>
//
//    var itemClickListener: ItemClickListener? = null
//
//
//    internal constructor(context: Context) {
//        this.context = context
//        items = ArrayList()
//        backup = ArrayList()
//        itemClickListener = null
//    }
//
//    internal constructor(context: Context, itemClickListener: ItemClickListener) {
//        this.context = context
//        items = ArrayList()
//        backup = ArrayList()
//        this.itemClickListener = itemClickListener
//    }
//
//
//
//    override fun onBindViewHolder(holder: Holder, position: Int) {
//        holder.bind(getItemAt(position))
//    }
//
//    override fun onViewRecycled(holder: Holder) {
//        holder.unbind()
//    }
//
//    override fun getItemCount(): Int {
//        return items.size
//    }
//
//    val backupItemsCount: Int
//        get() = backup.size
//
//    fun getItemAt(position: Int): E {
//        return items[position]
//    }
//
//    fun removeItemAt(position: Int) {
//        items.removeAt(position)
//        this.backup = items
//        notifyItemRemoved(position)
//    }
//
//    fun clear() {
//        items.clear()
//        this.backup = items
//        notifyDataSetChanged()
//    }
//
//    fun clearItem(position: Int) {
//        items.removeAt(position)
//        this.backup = items
//        notifyDataSetChanged()
//    }
//
//    fun restoreItem(deleteItem: E, position: Int) {
//        items.add(position, deleteItem)
//        this.backup = items
//        notifyItemInserted(position)
//    }
//
//    fun add(item: E) {
//        items.add(item)
//        this.backup = items
//        notifyItemInserted(itemCount - 1)
//    }
//
//    fun addFirst(item: E) {
//        items.add(0, item)
//        this.backup = items
//        notifyItemInserted(0)
//    }
//
//    fun addAll(items: List<E>) {
//        this.items.clear()
//        this.items.addAll(items)
//        this.backup = this.items
//        notifyDataSetChanged()
//    }
//
//    fun addAllNoClear(items: List<E>) {
//        this.items.addAll(items)
//        this.backup = this.items
//        notifyDataSetChanged()
//    }
//
//    fun updateItem(position: Int, item: E) {
//        items[position] = item
//        backup = items
//        notifyItemChanged(position)
//    }
////
////    fun updateVerticalItem(horizoontal: Int, vertical: Int) {
//////        notifyItemChanged(horizoontal);
////    }
//
//    fun getItemAtPosition(position: Int): E {
//        return items[position]
//    }
////
////    fun clearAll(items: List<E>) {
////        this.items.clear()
////    }
//
//    abstract class Holder<E> constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        open fun bind(item: E) {}
//        abstract fun unbind()
//    }
//}