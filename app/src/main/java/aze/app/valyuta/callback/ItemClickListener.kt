package aze.app.valyuta.callback

import android.view.View

interface ItemClickListener {
    fun onClick(v:View)
}