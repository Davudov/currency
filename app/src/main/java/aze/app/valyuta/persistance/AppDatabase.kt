package aze.app.valyuta.persistance

import androidx.databinding.adapters.Converters
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import aze.app.valyuta.entity.CurrencyEntity
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.persistance.currencies.CurrencyDao

@Database(entities = [CurrencyEntity::class], version = 3, exportSchema = false)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}