package aze.app.valyuta.persistance.currencies

import aze.app.valyuta.entity.CurrencyEntity
import io.reactivex.Completable
import io.reactivex.Single

interface CurrencyDataSource {
    fun saveCurrencies(currencies:CurrencyEntity):Single<Long>
    fun getCurrencies():Single<CurrencyEntity>
    fun getCurrency(date:String):Single<CurrencyEntity>
    fun updateCurrencies(currencies: CurrencyEntity):Completable
}