package aze.app.valyuta.persistance.currencies

import androidx.room.*
import aze.app.valyuta.entity.CurrencyEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface CurrencyDao {
    @Query("select * from currencies")
    fun getCurrencies(): Single<CurrencyEntity>

    @Query("select * from currencies where id_date = :date")
    fun getCurrency(date: String): Single<CurrencyEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveCurrencies(currencies: CurrencyEntity): Single<Long>

    @Update
    fun updateCurrencies(currencies: CurrencyEntity): Completable


}