package aze.app.valyuta.persistance.currencies

import aze.app.valyuta.entity.CurrencyEntity
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class LocalCurrency @Inject constructor(private val currencyDao: CurrencyDao) : CurrencyDataSource {

    override fun saveCurrencies(currencies: CurrencyEntity): Single<Long> =
        currencyDao.saveCurrencies(currencies)

    override fun getCurrencies(): Single<CurrencyEntity> = currencyDao.getCurrencies()

    override fun getCurrency(date: String): Single<CurrencyEntity> = currencyDao.getCurrency(date)

    override fun updateCurrencies(currencies: CurrencyEntity): Completable = currencyDao.updateCurrencies(currencies)

}