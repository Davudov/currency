package aze.app.valyuta.persistance

import android.os.Parcelable
import androidx.room.TypeConverter
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.entity.ValType
import aze.app.valyuta.entity.Valuate
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Converter {


    @TypeConverter
    fun fromStringValCurs(value: String): ValCurs {
        return Gson().fromJson(value, ValCurs::class.java)
    }

    @TypeConverter
    fun fromValCurs(valCurs: ValCurs): String? {
        return Gson().toJson(valCurs)
    }


    @TypeConverter
    fun fromArrayListValType(list: List<ValType>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun fromStringValType(value: String): List<ValType> {
        val listType =
            object : TypeToken<ArrayList<ValType>>() {}.type
        return Gson().fromJson<List<ValType>>(value, listType)
    }

    @TypeConverter
    fun fromArrayListValuate(list: List<Valuate>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun fromStringValuate(value: String): List<Valuate> {
        val listType =
            object : TypeToken<ArrayList<Valuate>>() {}.type
        return Gson().fromJson<List<Valuate>>(value, listType)
    }
}