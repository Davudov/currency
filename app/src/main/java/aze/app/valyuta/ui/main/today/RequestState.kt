package aze.app.valyuta.ui.main.today

import aze.app.valyuta.entity.CurrencyEntity
import aze.app.valyuta.entity.ValCurs

sealed class Result {
    data class SUCCESS(val currencyEntity: ValCurs) : Result()
    data class ERROR(val exception: Throwable) : Result()
    data class InProgress(val inProgress:Boolean) : Result()
}

