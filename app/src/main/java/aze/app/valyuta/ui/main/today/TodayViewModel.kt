package aze.app.valyuta.ui.main.today

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.EmptyResultSetException
import aze.app.valyuta.entity.CurrencyEntity
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.network.services.CurrencyService
import aze.app.valyuta.persistance.currencies.LocalCurrency
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TodayViewModel
@Inject constructor(
    private val currencyService: CurrencyService,
    private val localCurrency: LocalCurrency
) : ViewModel() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    val listState = MutableLiveData<Result>()

    private var today = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(Date())

    //getting today's currencies from database
    fun getTodayCurrency() {
        disposable.add(
            localCurrency.getCurrency(today)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(CurrencyEntity::valCurs)
                .subscribe({
                    onSuccess(it)
                }, {
                    onError(it)
                })
        )
    }

    private fun fetchTodayCurrencies() {
        disposable.add(currencyService.getTodayData(today)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onProgress(true) }
            .doAfterTerminate { onProgress(false) }
            .doOnSuccess(this::onSuccess)
            .map { CurrencyEntity(today, it) }
            .flatMap(this::saveTodayCurrencies)
            .subscribe({
                Timber.d("Saved to row $it")
            }, {
                Timber.d("Saving error $it")
            })
        )
    }

    private fun saveTodayCurrencies(currencyEntity: CurrencyEntity): Single<Long> {
        return localCurrency.saveCurrencies(currencies = currencyEntity)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSuccess { onSuccess(currencyEntity.valCurs) }
            .flatMap { Single.just(it) }
    }

    private fun onSuccess(responseBody: ValCurs) {
        listState.postValue(Result.SUCCESS(responseBody))
    }

    private fun onError(error: Throwable) {
        listState.postValue(Result.ERROR(error))
        if (error is EmptyResultSetException) {
            fetchTodayCurrencies()
        }
    }

    private fun onProgress(inProgress: Boolean) {
        listState.postValue(Result.InProgress(inProgress))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}