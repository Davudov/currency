package aze.app.valyuta.ui.main.screens

import androidx.fragment.app.Fragment
import aze.app.valyuta.ui.main.history.HistoryFragment
import aze.app.valyuta.ui.main.today.TodayFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {
    class TodayFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = TodayFragment()
    }

    class HistoryFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment = HistoryFragment()
    }

    class FragmentScreen<T>(private val t: T) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            if (t is Fragment) {
                return t
            } else return TodayFragment()
        }
    }
}