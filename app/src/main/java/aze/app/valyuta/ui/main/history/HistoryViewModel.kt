package aze.app.valyuta.ui.main.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import aze.app.valyuta.entity.CurrencyEntity
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.persistance.currencies.LocalCurrency
import aze.app.valyuta.ui.main.today.Result
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class HistoryViewModel
@Inject constructor(private val localCurrency: LocalCurrency) : ViewModel() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    val listState = MutableLiveData<Result>()


    //getting today's currencies from database
    fun getData(date:String) {
        disposable.add(
            localCurrency.getCurrency(date)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(CurrencyEntity::valCurs)
                .subscribe({
                    onSuccess(it)
                }, {
                    onError(it)
                })
        )
    }


    private fun onSuccess(responseBody: ValCurs) {
        listState.postValue(Result.SUCCESS(responseBody))
    }

    private fun onError(error: Throwable) {
        listState.postValue(Result.ERROR(error))
    }

}