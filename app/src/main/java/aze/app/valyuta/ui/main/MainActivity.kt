package aze.app.valyuta.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import aze.app.valyuta.R
import aze.app.valyuta.ui.main.history.HistoryFragment
import aze.app.valyuta.ui.main.screens.Screens
import aze.app.valyuta.ui.main.today.TodayFragment
import com.google.android.material.navigation.NavigationView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    private val navigator = SupportAppNavigator(this, R.id.frgmCont)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        router.replaceScreen(Screens.TodayFragmentScreen())
        nav_view.setNavigationItemSelectedListener(this)
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is TodayFragment) {
            toolbar.title = "Today currencies"
        }else if (fragment is HistoryFragment) {
            toolbar.title = "Stored data"
        }
        super.onAttachFragment(fragment)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        when (menuItem.itemId) {
            R.id.nav_today -> {
                router.replaceScreen(Screens.TodayFragmentScreen())
            }
            R.id.nav_history -> {
                router.navigateTo(Screens.HistoryFragmentScreen())
            }

        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (getCurrentFragment() is TodayFragment) {
            finish()
        } else {
            router.replaceScreen(Screens.TodayFragmentScreen())
        }

    }

    private fun getCurrentFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.frgmCont)!!
    }

}