package aze.app.valyuta.ui.main.today

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import aze.app.valyuta.R
import aze.app.valyuta.callback.ItemClickListener
import aze.app.valyuta.databinding.FragmentTodayBinding
import aze.app.valyuta.entity.Icons
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.factory.AppViewModelFactory
import aze.app.valyuta.recycler_item.ValTypeItem
import aze.app.valyuta.recycler_item.ValuateItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

class TodayFragment : DaggerFragment(), ItemClickListener {

    private lateinit var binding: FragmentTodayBinding

    private val adapter = GroupAdapter<ViewHolder>()
//    private lateinit var adapter:ValTypesAdapter

    @Inject
    lateinit var icons: Icons

    @Inject
    lateinit var providerFactory: AppViewModelFactory

    private val todayViewModel by lazy {
        ViewModelProvider(this, providerFactory).get(TodayViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_today, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        observeDataChange()
        todayViewModel.getTodayCurrency()
    }


    private fun initAdapter() {
//        adapter= ValTypesAdapter(context!!,itemClickListener = this)
        binding.fragmentTodayRecyclerView.itemAnimator = null
        binding.fragmentTodayRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.fragmentTodayRecyclerView.adapter = adapter
    }

    private fun observeDataChange() {
        todayViewModel.listState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.SUCCESS -> {
                    fillData(it.currencyEntity)
                }
                is Result.ERROR -> {
                    Timber.e(it.exception)
                }
                is Result.InProgress -> {
                    binding.showLoading = it.inProgress
                }
            }
        })
    }

    private fun fillData(currencyEntity: ValCurs) {
//        adapter.addAll(currencyEntity.valTypes)
//        currencyEntity.valTypes.forEach {
//            adapter.add(ValTypeItem(it.type))
//            it.valuateList.forEach { valuate ->
//                adapter.add(ValuateItem(valuate))
//            }
//        }

        adapter.add(ValTypeItem(currencyEntity.valTypes[0].type))

        currencyEntity.valTypes[0].valuateList.forEach { valuate ->
            adapter.add(ValuateItem(valuate,ContextCompat.getDrawable(context!!,icons.hashMap[valuate.code]!!)!!))
        }

        adapter.add(ValTypeItem(currencyEntity.valTypes[1].type))

        currencyEntity.valTypes[1].valuateList.forEach { valuate ->
            adapter.add(ValuateItem(valuate,ContextCompat.getDrawable(context!!,icons.hashMap[valuate.code]!!)!!))
        }


    }

    override fun onClick(v: View) {
        Toast.makeText(context!!, "Click", Toast.LENGTH_SHORT).show()
    }
}