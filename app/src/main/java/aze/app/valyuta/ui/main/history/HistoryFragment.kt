package aze.app.valyuta.ui.main.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.EmptyResultSetException
import aze.app.valyuta.R
import aze.app.valyuta.databinding.FragmentHistoryBinding
import aze.app.valyuta.entity.Icons
import aze.app.valyuta.entity.ValCurs
import aze.app.valyuta.factory.AppViewModelFactory
import aze.app.valyuta.recycler_item.EmptyResultItem
import aze.app.valyuta.recycler_item.ValTypeItem
import aze.app.valyuta.recycler_item.ValuateItem
import aze.app.valyuta.ui.main.today.Result
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_history.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class HistoryFragment : DaggerFragment(), CalendarView.OnDateChangeListener {

    private lateinit var binding: FragmentHistoryBinding

    private val adapter = GroupAdapter<ViewHolder>()

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    @Inject
    lateinit var providerFactory: AppViewModelFactory

    @Inject
    lateinit var icons: Icons

    private val historyViewModel by lazy {
        ViewModelProvider(this, providerFactory).get(HistoryViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                layoutInflater, R.layout.fragment_history, container, false
            )
        binding.viewModel = historyViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeDataChange()
    }


    private fun initViews() {
        binding.fragmentHistoryRecyclerView.itemAnimator = null
        binding.fragmentHistoryRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.fragmentHistoryRecyclerView.adapter = adapter
        binding.calendar.setOnDateChangeListener(this)
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
    }

    private fun observeDataChange() {
        historyViewModel.listState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.SUCCESS -> {
                    fillData(it.currencyEntity)
                }
                is Result.ERROR -> {
                    Timber.e(it.exception)
                    if (it.exception is EmptyResultSetException) {
                        adapter.clear()
                        adapter.add(EmptyResultItem())
                    }
                }
            }
        })
    }

    private fun fillData(currencyEntity: ValCurs) {
        adapter.clear()
        adapter.add(ValTypeItem(currencyEntity.valTypes[0].type))

        currencyEntity.valTypes[0].valuateList.forEach { valuate ->
            adapter.add(
                ValuateItem(
                    valuate,
                    ContextCompat.getDrawable(context!!, icons.hashMap[valuate.code]!!)!!
                )
            )
        }

        adapter.add(ValTypeItem(currencyEntity.valTypes[1].type))

        currencyEntity.valTypes[1].valuateList.forEach { valuate ->
            adapter.add(
                ValuateItem(
                    valuate,
                    ContextCompat.getDrawable(context!!, icons.hashMap[valuate.code]!!)!!
                )
            )
        }


    }

    override fun onSelectedDayChange(view: CalendarView, year: Int, month: Int, dayOfMonth: Int) {
        val cal = Calendar.getInstance()
        cal[Calendar.YEAR] = year
        cal[Calendar.MONTH] = month
        cal[Calendar.DAY_OF_MONTH] = dayOfMonth
        val dateRepresentation = cal.time
        val sdf = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(dateRepresentation)
        historyViewModel.getData(sdf.format())
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }


}
