package aze.app.valyuta

import android.app.Application
import aze.app.valyuta.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class App: DaggerApplication(){

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        initTimber()
        return DaggerAppComponent.builder().application(this).build()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}