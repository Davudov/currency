package aze.app.valyuta.recycler_item

import android.graphics.drawable.Drawable
import aze.app.valyuta.R
import aze.app.valyuta.entity.Valuate
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_valuate.view.*

class ValuateItem(private val valuateItem: Valuate,private val icon:Drawable) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val value = "${valuateItem.value} AZN"
        viewHolder.itemView.code.text = valuateItem.code
        viewHolder.itemView.name.text = valuateItem.name
        viewHolder.itemView.value.text = value
        viewHolder.itemView.icon.setImageDrawable(icon)
    }

    override fun getLayout(): Int = R.layout.item_valuate
}