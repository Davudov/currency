package aze.app.valyuta.recycler_item

import aze.app.valyuta.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_val_type.view.*

class ValTypeItem(private val valType: String) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.type.text = valType
    }

    override fun getLayout(): Int = R.layout.item_val_type

}