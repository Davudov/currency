package aze.app.valyuta.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Entity(tableName = "currencies")
class CurrencyEntity @JvmOverloads constructor(
    @PrimaryKey
    val id_date: String,
    @Embedded
    val valCurs: ValCurs
)
@Root(name = "ValCurs", strict = false)
data class ValCurs @JvmOverloads constructor(
    @field:Attribute(name = "Date")
    var date: String = "",
    @field:ElementList(entry = "ValType", inline = true)
    var valTypes: List<ValType> = ArrayList()
)

data class ValType @JvmOverloads constructor(
    @field:Attribute(name = "Type")
    var type: String = "",
    @field:ElementList(entry = "Valute", inline = true)
    var valuateList: List<Valuate> = ArrayList()
)

data class Valuate @JvmOverloads constructor(
    @field:Attribute(name = "Code")
    var code: String = "",
    @field:Element(name = "Nominal")
    var nominal: String = "",
    @field:Element(name = "Name")
    var name: String = "",
    @field:Element(name = "Value")
    var value: Double = .0
)
