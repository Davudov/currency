package aze.app.valyuta.entity

import aze.app.valyuta.R

class Icons {
    val hashMap = HashMap<String, Int>()
    init {
        hashMap["XPD"] = R.drawable.ic_palladium
        hashMap["XPT"] = R.drawable.ic_platinum
        hashMap["XAG"] = R.drawable.ic_silver
        hashMap["XAU"] = R.drawable.ic_aurum
        hashMap["USD"] = R.drawable.ic_usa
        hashMap["EUR"] = R.drawable.ic_europe
        hashMap["AUD"] = R.drawable.ic_australia
        hashMap["ARS"] = R.drawable.ic_argentina
        hashMap["BYN"] = R.drawable.ic_belarus
        hashMap["BRL"] = R.drawable.ic_brazil
        hashMap["AED"] = R.drawable.ic_saudi_arabia
        hashMap["ZAR"] = R.drawable.ic_south_africa
        hashMap["KRW"] = R.drawable.ic_south_korea
        hashMap["CZK"] = R.drawable.ic_czech_republic
        hashMap["CLP"] = R.drawable.ic_chile
        hashMap["CNY"] = R.drawable.ic_china
        hashMap["DKK"] = R.drawable.ic_denmark
        hashMap["GEL"] = R.drawable.ic_georgia
        hashMap["HKD"] = R.drawable.ic_hong_kong
        hashMap["INR"] = R.drawable.ic_india
        hashMap["GBP"] = R.drawable.ic_uk
        hashMap["IDR"] = R.drawable.ic_indonesia
        hashMap["IRR"] = R.drawable.ic_iran
        hashMap["SEK"] = R.drawable.ic_sweden
        hashMap["CHF"] = R.drawable.ic_switzerland
        hashMap["ILS"] = R.drawable.ic_israel
        hashMap["CAD"] = R.drawable.ic_canada
        hashMap["KWD"] = R.drawable.ic_kuwait
        hashMap["KZT"] = R.drawable.ic_kazakhstan
        hashMap["KGS"] = R.drawable.ic_kyrgyzstan
        hashMap["LBP"] = R.drawable.ic_lebanon
        hashMap["MYR"] = R.drawable.ic_malaysia
        hashMap["MXN"] = R.drawable.ic_mexico
        hashMap["MDL"] = R.drawable.ic_moldova
        hashMap["EGP"] = R.drawable.ic_egypt
        hashMap["NOK"] = R.drawable.ic_norway
        hashMap["UZS"] = R.drawable.ic_uzbekistan
        hashMap["PLN"] = R.drawable.ic_poland
        hashMap["RUB"] = R.drawable.ic_russia
        hashMap["SGD"] = R.drawable.ic_singapore
        hashMap["SAR"] = R.drawable.ic_saudi_arabia
        hashMap["SDR"] = R.drawable.sdr_currency_exchange
        hashMap["TRY"] = R.drawable.ic_turkey
        hashMap["TWD"] = R.drawable.ic_taiwan
        hashMap["TJS"] = R.drawable.ic_tajikistan
        hashMap["TMT"] = R.drawable.ic_turkmenistan
        hashMap["UAH"] = R.drawable.ic_ukraine
        hashMap["JPY"] = R.drawable.ic_japan
        hashMap["NZD"] = R.drawable.ic_new_zealand
    }
}